package com.example.diogokiss;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringJoiner;

public class Formatter {
    private Integer width;
    private String alignment;
    private int spacewidth = " ".length();
    private static String[] supportedAlignments = new String[] { "left", "right", "center", "justify" };
    // TODO: Dinamically figure out the supported alignments
    public static String usage = String.format(
            "usage: java -jar formatter.jar <alignment> <output width>\n" +
            "  <alignment>: %s\n" +
            "  <output widht>: maximum number of characters per line\n",String.join(",", Formatter.supportedAlignments));

    public String getAlignment() {
        return alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public int getSpacewidth() {
        return spacewidth;
    }

    public void setSpacewidth(int spacewidth) {
        this.spacewidth = spacewidth;
    }

    public static Boolean validateArgs(String[] args) {

        if (args.length == 0) {
            return false;
        }

        if (args.length != 2) {
            System.out.format("Wrong number of arguments! Expected 2, given: %s.\n",args.length);
            return false;
        }

        if (!Arrays.asList(Formatter.supportedAlignments).contains(args[0])) {
            System.out.format("'%s' alignment is not supported.\n",args[0]);
            return false;
        }

        try { 
            Integer.parseInt(args[1]);
        } catch(NumberFormatException e) { 
            System.out.format("'%s' is not an integer.\n",args[1]);
            return false; 
        }

        return true;
    }

    public String readInput() throws IOException {
        StringBuilder text = new StringBuilder();
        
        Scanner stdin = new Scanner(new BufferedInputStream(System.in));
        String line = new String();
        while (stdin.hasNext()) {
            line = stdin.nextLine();
            text.append(line);
            text.append(" ");
        }
        stdin.close();

        return text.toString();
    }

    public boolean inspectWordSize(String text) {
        ArrayList<String> words = new ArrayList<String>(Arrays.asList(text.split(" ")));
        String biggestword = new String();
        for (String word: words) {
            if (word.length() > biggestword.length())
                biggestword = word;
        }

        if (biggestword.length() > this.getWidth()) {
            System.out.format("\nThis software does not support word wrapping and the word '%s' is bigger than the supplied line width (%s). Please set 'width' parameter to its size (%s)\n",biggestword, this.getWidth(), biggestword.length());
            return false;
        }

        return true;
    }

    public String processLine(String line){
        switch(this.getAlignment()) {
            case "left":
                return this.left(line);
            case "right":
                return this.right(line);
            case "center":
                return this.center(line);
            case "justify":
                return this.justify(line);
            default:
                return line;
        }
    }
    
    public String left(String text) {
        return text;
    }

    public String right(String text) {
        String trimmed = text.trim();
        int spaceleft = this.getWidth() - trimmed.length(); 
        String spaces = new String(new char[spaceleft]).replace("\0", " ");
        String rightline = spaces + trimmed;
        return rightline;
    }

    public String center(String text) {
        String trimmed = text.trim();
        int spaceleft = this.getWidth() - trimmed.length();
        int spacesbefore = (int) Math.ceil(spaceleft/2);
        int spacesafter = (int) Math.floor(spaceleft/2);
        String spacesleft = new String(new char[spacesbefore]).replace("\0", " ");
        String spacesright = new String(new char[spacesafter]).replace("\0", " ");
        String centerline = spacesleft + trimmed + spacesright;
        return centerline;
    }

    public String justify(String text) {
        String trimmed = text.trim();

        // There is just one word, so we can't insert spaces.
        if (trimmed.split(" ").length < 2)
            return text;
        
        int spaceleft = this.getWidth() - trimmed.length();
        StringBuilder st = new StringBuilder();
        
        char[] characters = trimmed.toCharArray();
        while (spaceleft > 0) {
            
            for(int i=0; i < characters.length; i++){
                if (characters[i] != ' '){
                   st.append(characters[i]);
                }
                
                if (characters[i] == ' '){
                    if ( (characters[i+1] != ' ') && (spaceleft > 0) ) {
                        st.append("  "); //Increase spacing by 1
                        spaceleft--;
                    } else {
                        st.append(' ');  //Maintain spacing
                    }
                }
            }

            characters = st.toString().toCharArray();
            st = new StringBuilder();
        }
        
        return new String(characters);
    }

    public String processText(String text) {
        String[] words = Arrays.stream(text.split(" ")).filter(s -> !s.isEmpty()).toArray(String[]::new);
        StringBuilder output = new StringBuilder();
        StringJoiner line = new StringJoiner(" ", "", "");
        int maxlinesize = this.getWidth();
      
        for (String word: words) {
            if (line.toString().length() + word.length() + this.getSpacewidth() >= maxlinesize) {
                output.append(this.processLine(line.toString()));
                output.append("\n");

                line = new StringJoiner(" ", "", "");
            }

            line.add(word);
        }
        if (line.length() > 0) {
            output.append(this.processLine(line.toString()));
            output.append("\n");
        }
        
        return output.toString();
    }

    public static void main(String[] args) {
        if (Formatter.validateArgs(args)) {
            String alignment = args[0].toLowerCase();
            Integer width = Integer.parseInt(args[1]);

            Formatter f = new Formatter();
            f.setAlignment(alignment);
            f.setWidth(width);
            String read;
            
            try {
                read = f.readInput();
                if (!f.inspectWordSize(read))
                   System.exit(1);

                System.out.println();
                System.out.println(f.processText(read));

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println();
            System.out.println(Formatter.usage);
        }

    }
}
