package com.example.diogokiss;

import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for Formatter.
 */
public class FormatterTest 
    extends TestCase
{
    
    private String inputText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus. Pauca mutat vel plura sane; Dat enim intervalla et relaxat. Scisse enim te quis coarguere possit? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a te dicta sunt. tulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. De hominibus dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Quia dolori non voluptas contraria est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur.";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FormatterTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( FormatterTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testInputEqualsToOutputLeftAlignment()
    {
        Formatter f = new Formatter();
        f.setAlignment("left");
        f.setWidth(80);
        
        String input = this.inputText;
        String output = f.processText(input);

        String[] inputWords = input.split(" ");
        String[] outputWords = output.replaceAll("\n"," ").split(" ");
      
        inputWords = Arrays.stream(inputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);
        outputWords = Arrays.stream(outputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);

        assertEquals(inputWords.length,outputWords.length);
        
        for(int i=0; i<inputWords.length; i++){
            assertEquals(inputWords[i].compareTo(outputWords[i]),0);
        }
    }

    public void testInputEqualsToOutputRightAlignment()
    {
        Formatter f = new Formatter();
        f.setAlignment("right");
        f.setWidth(80);
        
        String input = this.inputText;
        String output = f.processText(input);

        String[] inputWords = input.split(" ");
        String[] outputWords = output.replaceAll("\n"," ").split(" ");
      
        inputWords = Arrays.stream(inputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);
        outputWords = Arrays.stream(outputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);

        assertEquals(inputWords.length,outputWords.length);
        
        for(int i=0; i<inputWords.length; i++){
            assertEquals(inputWords[i].compareTo(outputWords[i]),0);
        }
    }
    
    public void testInputEqualsToOutputCenterAlignment()
    {
        Formatter f = new Formatter();
        f.setAlignment("center");
        f.setWidth(80);
        
        String input = this.inputText;
        String output = f.processText(input);

        String[] inputWords = input.replace("\n"," ").split(" ");
        String[] outputWords = output.replace("\n"," ").split(" ");
      
        inputWords = Arrays.stream(inputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);
        outputWords = Arrays.stream(outputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);

        assertEquals(inputWords.length,outputWords.length);
        
        for(int i=0; i<inputWords.length; i++){
            assertEquals(inputWords[i].compareTo(outputWords[i]),0);
        }
    }
    
    public void testInputEqualsToOutputJustifyAlignment()
    {
        Formatter f = new Formatter();
        f.setAlignment("justify");
        f.setWidth(80);
        
        String input = this.inputText;
        String output = f.processText(input);

        String[] inputWords = input.split(" ");
        String[] outputWords = output.replace("\n"," ").replaceAll("\\s+", " ").split(" ");

        inputWords = Arrays.stream(inputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);
        outputWords = Arrays.stream(outputWords).filter(s -> !s.isEmpty()).toArray(String[]::new);

        assertEquals(inputWords.length,outputWords.length);
        
        for(int i=0; i<inputWords.length; i++){
            assertEquals(inputWords[i].compareTo(outputWords[i]),0);
        }
    }

    public void testLeft()
    {
        Formatter f = new Formatter();
        f.setWidth(20);
        
        String input = "Lorem ipsum dolor";
        String output = f.left(input);
        String expected = "Lorem ipsum dolor";
        assertEquals(output,expected);
    }
    
    public void testRight()
    {
        Formatter f = new Formatter();
        f.setWidth(20);
        
        String input = "Lorem ipsum dolor";
        String output = f.right(input);
        String expected = "   Lorem ipsum dolor";
        assertEquals(output,expected);
    }
    
    public void testCenter()
    {
        Formatter f = new Formatter();
        f.setWidth(20);
        
        String input = "Lorem ipsum dolor";
        String output = f.center(input);
        String expected = " Lorem ipsum dolor ";
        assertEquals(output,expected);
    }
    
    public void testJustify()
    {
        Formatter f = new Formatter();
        f.setWidth(20);
        
        String input = "Lorem ipsum dolor";
        String output = f.justify(input);
        String expected = "Lorem   ipsum  dolor";
        assertEquals(output,expected);       
    }

    public void testProcessLineUnsupported() {
        Formatter f = new Formatter();
        f.setWidth(20);
        f.setAlignment("unsupported");
        
        String input = "Lorem ipsum dolor";
        String output = f.processLine(input);
        String expected = "Lorem ipsum dolor";
        assertEquals(output,expected);        
    }
    
    public void testProcessLineLeft() {
        Formatter f = new Formatter();
        f.setWidth(20);
        f.setAlignment("left");
        
        String input = "Lorem ipsum dolor";
        String output = f.processLine(input);
        String expected = "Lorem ipsum dolor";
        assertEquals(output,expected);        
    }
    
    public void testProcessLineRight() {
        Formatter f = new Formatter();
        f.setWidth(20);
        f.setAlignment("right");
        
        String input = "Lorem ipsum dolor";
        String output = f.processLine(input);
        String expected = "   Lorem ipsum dolor";
        assertEquals(output,expected);        
    }
    
    public void testProcessLineCenter() {
        Formatter f = new Formatter();
        f.setWidth(20);
        f.setAlignment("center");
        
        String input = "Lorem ipsum dolor";
        String output = f.processLine(input);
        String expected = " Lorem ipsum dolor ";
        assertEquals(output,expected);        
    }
    
    public void testProcessLineJustify() {
        Formatter f = new Formatter();
        f.setWidth(20);
        f.setAlignment("justify");
        
        String input = "Lorem ipsum dolor";
        String output = f.processLine(input);
        String expected = "Lorem   ipsum  dolor";
        assertEquals(output,expected);        
    }
    
    public void testvalidateArgs() {
        assertFalse(Formatter.validateArgs(new String[] {}));
        assertFalse(Formatter.validateArgs(new String[] { "", "" }));
        assertFalse(Formatter.validateArgs(new String[] { "left", "two" }));
        assertFalse(Formatter.validateArgs(new String[] { "20", "right" }));
        
        assertTrue(Formatter.validateArgs(new String[] { "left", "20" }));
        assertTrue(Formatter.validateArgs(new String[] { "right", "20" }));
        assertTrue(Formatter.validateArgs(new String[] { "center", "20" }));
        assertTrue(Formatter.validateArgs(new String[] { "justify", "20" }));
    }
    
    public void testinspectWordSize() {
        Formatter f = new Formatter();
        f.setWidth(10);
        assertFalse(f.inspectWordSize("consectetuer"));
        
        f.setWidth(15);
        assertTrue(f.inspectWordSize("consectetuer"));
    }

    public void testProcessTextLeft() {
        Formatter f = new Formatter();
        f.setWidth(80);
        f.setAlignment("left");
        
        String input = this.inputText;
        String output = f.processText(input);
        String expected = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,\ninfame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si\nplacet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem,\nconstantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,\nrevocat autem Antiochus, nec est praeterea, quem audiamus. Pauca mutat vel\nplura sane; Dat enim intervalla et relaxat. Scisse enim te quis coarguere\npossit? Septem autem illi non suo, sed populorum suffragio omnium nominati\nsunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia\nrespondere, quae a te dicta sunt. tulti autem malorum memoria torquentur,\nsapientes bona praeterita grata recordatione renovata delectant. De hominibus\ndici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum\net eundem supremum diem, scribebamus haec. Quia dolori non voluptas contraria\nest, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris;\nDuarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa\nnatura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea\nquicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec\nconiunctio confusioque virtutum tamen a philosophis ratione quadam\ndistinguitur.\n";
        assertEquals(output,expected);
    }
    
    public void testProcessTextRight() {
        Formatter f = new Formatter();
        f.setWidth(80);
        f.setAlignment("right");
        
        String input = this.inputText;
        String output = f.processText(input);
        String expected = "  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,\n     infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si\n   placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem,\n       constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,\n      revocat autem Antiochus, nec est praeterea, quem audiamus. Pauca mutat vel\n       plura sane; Dat enim intervalla et relaxat. Scisse enim te quis coarguere\n      possit? Septem autem illi non suo, sed populorum suffragio omnium nominati\n        sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia\n       respondere, quae a te dicta sunt. tulti autem malorum memoria torquentur,\n   sapientes bona praeterita grata recordatione renovata delectant. De hominibus\n   dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum\n   et eundem supremum diem, scribebamus haec. Quia dolori non voluptas contraria\n       est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris;\n   Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa\n      natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea\n       quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec\n              coniunctio confusioque virtutum tamen a philosophis ratione quadam\n                                                                   distinguitur.\n";
        assertEquals(output,expected);         
    }
    
    public void testProcessTextCenter() {
        Formatter f = new Formatter();
        f.setWidth(80);
        f.setAlignment("center");
        
        String input = this.inputText;
        String output = f.processText(input);
        String expected = " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, \n  infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si  \n placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, \n   constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,   \n   revocat autem Antiochus, nec est praeterea, quem audiamus. Pauca mutat vel   \n   plura sane; Dat enim intervalla et relaxat. Scisse enim te quis coarguere   \n   possit? Septem autem illi non suo, sed populorum suffragio omnium nominati   \n    sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia    \n   respondere, quae a te dicta sunt. tulti autem malorum memoria torquentur,   \n sapientes bona praeterita grata recordatione renovata delectant. De hominibus \n dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum \n et eundem supremum diem, scribebamus haec. Quia dolori non voluptas contraria \n   est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris;   \n Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa \n   natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea   \n   quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec   \n       coniunctio confusioque virtutum tamen a philosophis ratione quadam       \n                                 distinguitur.                                 \n";
        assertEquals(output,expected);         
    }
    
    public void testProcessTextJutify() {
        Formatter f = new Formatter();
        f.setWidth(80);
        f.setAlignment("justify");
        
        String input = this.inputText;
        String output = f.processText(input);
        String expected = "Lorem  ipsum  dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,\ninfame,  suspectum.  Age,  inquies,  ista  parva sunt. Sed residamus, inquit, si\nplacet.  Duo  Reges:  constructio interrete. Quis contra in illa aetate pudorem,\nconstantiam,  etiamsi  sua  nihil  intersit,  non  tamen  diligat? Rapior illuc,\nrevocat  autem  Antiochus,  nec  est  praeterea,  quem audiamus. Pauca mutat vel\nplura  sane;  Dat  enim  intervalla  et  relaxat.  Scisse enim te quis coarguere\npossit?  Septem  autem  illi  non  suo,  sed populorum suffragio omnium nominati\nsunt.  Oratio  me  istius  philosophi  non  offendit;  Longum  est enim ad omnia\nrespondere,  quae  a  te  dicta  sunt.  tulti  autem malorum memoria torquentur,\nsapientes  bona  praeterita  grata recordatione renovata delectant. De hominibus\ndici  non  necesse  est. At eum nihili facit; Cum ageremus, inquit, vitae beatum\net  eundem  supremum  diem, scribebamus haec. Quia dolori non voluptas contraria\nest,  sed  doloris  privatio.  Summum  ením  bonum  exposuit vacuitatem doloris;\nDuarum  enim  vitarum  nobis erunt instituta capienda. Sapientem locupletat ipsa\nnatura,  cuius  divitias  Epicurus  parabiles  esse  docuit. Cum autem negant ea\nquicquam  ad  beatam  vitam  pertinere,  rursus  naturam  relinquunt. Atque haec\nconiunctio   confusioque   virtutum   tamen   a   philosophis   ratione   quadam\ndistinguitur.\n";
        assertEquals(output,expected);         
    }
    
}
