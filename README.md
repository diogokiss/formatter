# Formatter

**Formatter** is a simple text formatting tool.
It reads some text from the standard input (use `CTRL+D` to signal EOF) and formats it according to the *parameters* passed to it.

## Usage examples

```
$ java -jar target/formatter.jar 

usage: java -jar formatter.jar <alignment> <output width>
  <alignment>: left,right,center,justify
  <output widht>: maximum number of characters per line
```

## Supported alignment types

### Left 
```
$ java -jar target/formatter.jar left 80
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus.
Pauca mutat vel plura sane; Dat enim intervalla et relaxat.
Scisse enim te quis coarguere possit? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a te dicta sunt.
tulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. De hominibus dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
Quia dolori non voluptas contraria est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,
infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si
placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem,
constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,
revocat autem Antiochus, nec est praeterea, quem audiamus.Pauca mutat vel plura
sane; Dat enim intervalla et relaxat.Scisse enim te quis coarguere possit?
Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio
me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a
te dicta sunt.tulti autem malorum memoria torquentur, sapientes bona praeterita
grata recordatione renovata delectant. De hominibus dici non necesse est. At
eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem,
scribebamus haec.Quia dolori non voluptas contraria est, sed doloris privatio.
Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt
instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus
parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere,
rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a
philosophis ratione quadam distinguitur.
``` 

### Right
```
$ java -jar target/formatter.jar right 80
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus.
Pauca mutat vel plura sane; Dat enim intervalla et relaxat.
Scisse enim te quis coarguere possit? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a te dicta sunt.
tulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. De hominibus dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
Quia dolori non voluptas contraria est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur.

  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,
     infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si
   placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem,
       constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,
 revocat autem Antiochus, nec est praeterea, quem audiamus.Pauca mutat vel plura
      sane; Dat enim intervalla et relaxat.Scisse enim te quis coarguere possit?
 Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio
  me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a
 te dicta sunt.tulti autem malorum memoria torquentur, sapientes bona praeterita
    grata recordatione renovata delectant. De hominibus dici non necesse est. At
   eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem,
  scribebamus haec.Quia dolori non voluptas contraria est, sed doloris privatio.
  Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt
   instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus
  parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere,
   rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a
                                        philosophis ratione quadam distinguitur.
```

### Center
```
$ java -jar target/formatter.jar center 80
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus.
Pauca mutat vel plura sane; Dat enim intervalla et relaxat.
Scisse enim te quis coarguere possit? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a te dicta sunt.
tulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. De hominibus dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
Quia dolori non voluptas contraria est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur.

 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, 
  infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si  
 placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, 
   constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc,   
revocat autem Antiochus, nec est praeterea, quem audiamus.Pauca mutat vel plura
   sane; Dat enim intervalla et relaxat.Scisse enim te quis coarguere possit?   
Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio
 me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a 
te dicta sunt.tulti autem malorum memoria torquentur, sapientes bona praeterita
  grata recordatione renovata delectant. De hominibus dici non necesse est. At  
 eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, 
 scribebamus haec.Quia dolori non voluptas contraria est, sed doloris privatio. 
 Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt 
 instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus 
 parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, 
 rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a 
                    philosophis ratione quadam distinguitur.
```

### Justify
```
$ java -jar target/formatter.jar justify 80
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est, infame, suspectum. Age, inquies, ista parva sunt. Sed residamus, inquit, si placet. Duo Reges: constructio interrete. Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus.
Pauca mutat vel plura sane; Dat enim intervalla et relaxat.
Scisse enim te quis coarguere possit? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio me istius philosophi non offendit; Longum est enim ad omnia respondere, quae a te dicta sunt.
tulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. De hominibus dici non necesse est. At eum nihili facit; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
Quia dolori non voluptas contraria est, sed doloris privatio. Summum ením bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt instituta capienda. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur.

Lorem  ipsum  dolor sit amet, consectetur adipiscing elit. Invidiosum nomen est,
infame,  suspectum.  Age,  inquies,  ista  parva sunt. Sed residamus, inquit, si
placet.  Duo  Reges:  constructio interrete. Quis contra in illa aetate pudorem,
constantiam,  etiamsi  sua  nihil  intersit,  non  tamen  diligat? Rapior illuc,
revocat  autem Antiochus, nec est praeterea, quem audiamus.Pauca mutat vel plura
sane;  Dat  enim  intervalla  et  relaxat.Scisse  enim te quis coarguere possit?
Septem  autem illi non suo, sed populorum suffragio omnium nominati sunt. Oratio
me  istius  philosophi non offendit; Longum est enim ad omnia respondere, quae a
te  dicta sunt.tulti autem malorum memoria torquentur, sapientes bona praeterita
grata  recordatione  renovata  delectant.  De hominibus dici non necesse est. At
eum  nihili  facit;  Cum ageremus, inquit, vitae beatum et eundem supremum diem,
scribebamus  haec.Quia  dolori non voluptas contraria est, sed doloris privatio.
Summum  ením  bonum exposuit vacuitatem doloris; Duarum enim vitarum nobis erunt
instituta  capienda.  Sapientem  locupletat ipsa natura, cuius divitias Epicurus
parabiles  esse  docuit. Cum autem negant ea quicquam ad beatam vitam pertinere,
rursus  naturam  relinquunt.  Atque haec coniunctio confusioque virtutum tamen a
philosophis               ratione              quadam              distinguitur.
```

## How to build

In order to generate the `jar` file, use the following command:
`$ mvn clean compile package`

It will generate the file in the following path after going throw the unit tests:
`target/formatter.jar`

